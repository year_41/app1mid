package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val Hellobutton: Button = findViewById<Button>(R.id.button_hello)
        Hellobutton.setOnClickListener {
            val intent = Intent (this,HelloActivity::class.java)
            intent.putExtra("Name1","Thirawat Charoenruen")
            startActivity(intent)

            val nameTextView: TextView = findViewById(R.id.Name)
            val idTextView: TextView = findViewById(R.id.txt_id)
            Log.d(TAG,""+nameTextView.text)
            Log.d(TAG,""+idTextView.text)
            setContentView(R.layout.activity_hello)

        }
        supportActionBar!!.title= "HELLO"


    }
}


